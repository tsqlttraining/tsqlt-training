﻿USE tSQLtTraining_M2

IF OBJECT_ID('AssertionsTests.[tests if ProductsInShipment has correct schemata]') IS NOT NULL 
DROP PROCEDURE AssertionsTests.[tests if ProductsInShipment has correct schemata]
GO 

CREATE PROCEDURE AssertionsTests.[tests if ProductsInShipment has correct schemata]
AS
BEGIN
	IF OBJECT_ID('actual') IS NOT NULL
		DROP TABLE actual;
	IF OBJECT_ID('expected') IS NOT NULL
		DROP TABLE expected;
    
	   --dokończ przygotowywanie danych
    
	-- When

	   --pobierz dane z widoku

	-- Then	
	EXEC tSQLt.AssertEqualsTableSchema 'expected'
		,'actual'
END
GO

EXEC tSQLt.Run 'AssertionsTests.[tests if ProductsInShipment has correct schemata]'