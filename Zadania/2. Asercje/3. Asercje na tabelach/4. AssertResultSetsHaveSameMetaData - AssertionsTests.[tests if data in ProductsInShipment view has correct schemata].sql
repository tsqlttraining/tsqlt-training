﻿USE tSQLtTraining_M2

IF OBJECT_ID('AssertionsTests.[tests if data in ProductsInShipment view has correct schemata]') IS NOT NULL
	DROP PROCEDURE AssertionsTests.[tests if data in ProductsInShipment view has correct schemata]
GO

CREATE PROCEDURE AssertionsTests.[tests if data in ProductsInShipment view has correct schemata]
AS
BEGIN    
	DECLARE @expected nvarchar(max);
	DECLARE @actual nvarchar(max);
	-- Given

	   --dokończ przygotowywanie danych
	-- When

	   --pobierz dane z widoku

	-- Then
	EXEC tSQLt.AssertResultSetsHaveSameMetaData @expected
		,@actual
END
GO

EXEC tSQLt.Run 'AssertionsTests.[tests if data in ProductsInShipment view has correct schemata]'
