﻿USE tSQLtTraining_M2

IF OBJECT_ID('AssertionsTests.[tests if ProductsInShipment returns correct data]') IS NOT NULL 
DROP PROCEDURE AssertionsTests.[tests if ProductsInShipment returns correct data]
GO 

CREATE PROCEDURE AssertionsTests.[tests if ProductsInShipment returns correct data]
AS
BEGIN
	IF OBJECT_ID('actual') IS NOT NULL
		DROP TABLE actual;
	IF OBJECT_ID('expected') IS NOT NULL
		DROP TABLE expected;

	-- Given
    -- dokończ przygotowywanie danych

	-- When
	SELECT *	
	INTO actual
	FROM Storage.ProductsInShipment

	-- Then
	CREATE TABLE expected (
		ProductName NVARCHAR(60),
		Quantity INT,
		DispatchedOn DATETIME2,
	)

	INSERT INTO expected
	VALUES ('Rubber duck', 42, '2016-11-02 11:32')
		,('Scissors', 30, '2016-11-03 12:45')

	-- Then	
	EXEC tSQLt.AssertEqualsTable 'expected'
		,'actual'
END
GO

EXEC tSQLt.Run 'AssertionsTests.[tests if ProductsInShipment returns correct data]'