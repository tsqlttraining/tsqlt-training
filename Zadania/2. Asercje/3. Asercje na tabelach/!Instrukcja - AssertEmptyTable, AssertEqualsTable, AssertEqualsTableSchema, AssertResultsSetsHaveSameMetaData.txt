﻿UWAGA! w ćwiczeniach wykorzystujemy bazę tSQLtTraining_M2

 1. AssertEmptyTable - w klasie testowej AssertionsTests stwórz dowolny test sprawdzający czy któraś z tabel jest pusta np. Production.Product. Możesz wykorzystać szablon testu:
	
	   AssertEmptyTable - AssertionsTests.[tests if Production.Product table is empty]

 2. AssertEqualsTable - tej asercji można użyć także do sprawdzania danych zwracanych z widoków. Napisz test sprawdzający czy widok Storage.ProductsInShipment zwraca poprawne dane po dodaniu wpisów do odpowiednich tabel. Możesz wykorzystać szablon testu:
	
	   AssertEqualsTable - AssertionsTests.[tests if ProductsInShipment returns correct data]

3. AssertEqualsTableSchema - tej asercji również można użyć do sprawdzania danych zwracanych z widoków. Napisz test sprawdzający czy widok z poprzedniego testu - Storage.ProductsInShipment zwraca dane o poprawnej strukturze. Możesz wykorzystać szablon testu:
	
	   AssertEqualsTableSchema - AssertionsTests.[tests if ProductsInShipment has correct schemata]

4. AssertResultSetsHaveSameMetaData - sprawdź czy widok Storage.ProductsInShipment zwraca dane do raportu w takim samym formacie w jakim są zapisane w tableach w bazie. Możesz wykorzystać szablon testu:
    
    	   AssertResultSetsHaveSameMetaData - AssertionsTests.[tests if data in ProductsInShipment view has correct schemata]