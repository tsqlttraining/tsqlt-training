USE tSQLtTraining_M2

IF OBJECT_ID('AssertionsTests.[tests if utils.Max returns parameter with higher value]') IS NOT NULL 
DROP PROCEDURE AssertionsTests.[tests if utils.Max returns parameter with higher value]
GO 

CREATE PROCEDURE AssertionsTests.[tests if utils.Max returns parameter with higher value]
AS
BEGIN
	-- Given

	-- When

	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END
GO

EXEC tSQLt.Run 'AssertionsTests.[tests if utils.Max returns parameter with higher value]'

IF OBJECT_ID('AssertionsTests.[tests if utils.Min returns parameter with lower value]') IS NOT NULL 
DROP PROCEDURE AssertionsTests.[tests if utils.Min returns parameter with lower value]
GO 

CREATE PROCEDURE AssertionsTests.[tests if utils.Min returns parameter with lower value]
AS
BEGIN
	-- Given

	-- When

	-- Then	
	EXEC tSQLt.Fail 'Not implemented'
END
GO

EXEC tSQLt.Run 'AssertionsTests.[tests if utils.Min returns parameter with lower value]'