USE tSQLtTraining_M2

IF OBJECT_ID('AssertionsTests.[test exception is raised when adding non-unique Product]') IS NOT NULL 
DROP PROCEDURE AssertionsTests.[test exception is raised when adding non-unique Product]
GO 

CREATE PROCEDURE AssertionsTests.[test exception is raised when adding non-unique Product]
AS
BEGIN
	-- Given

	-- When

	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END
GO

EXEC tSQLt.Run 'AssertionsTests.[test exception is raised when adding non-unique Product]'