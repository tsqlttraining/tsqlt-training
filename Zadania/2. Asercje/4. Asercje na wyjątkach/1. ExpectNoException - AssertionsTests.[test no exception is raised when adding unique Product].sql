USE tSQLtTraining_M2

IF OBJECT_ID('AssertionsTests.[test no exception is raised when adding unique Product]') IS NOT NULL 
DROP PROCEDURE AssertionsTests.[test no exception is raised when adding unique Product]
GO 

CREATE PROCEDURE AssertionsTests.[test no exception is raised when adding unique Product]
AS
BEGIN
	-- Given

	-- When

	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END
GO

EXEC tSQLt.Run 'AssertionsTests.[test no exception is raised when adding unique Product]'
