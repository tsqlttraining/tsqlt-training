USE tSQLtTraining_M3

IF OBJECT_ID('Storage_WarehouseInventoryTests.[test that it logs when product is deleted from inventory]') IS NOT NULL
DROP PROCEDURE Storage_WarehouseInventoryTests.[test that it logs when product is deleted from inventory]
GO

CREATE PROCEDURE Storage_WarehouseInventoryTests.[test that it logs when product is deleted from inventory]
AS
BEGIN
	-- Przetestuj czy odpowiednie wiersze s� dodawane do tabeli Storage.WarehouseInventoryHistory
	-- kiedy s� usuwane s� wiersze do tabeli Storage.WarehouseInventory przez trigger

	-- postaraj si� r�wnocze�nie nie testowa� czy wiersze s� dodawane podczas wstawiania danych do tabeli
	-- (w tabeli Storage.WarehouseInventoryHistory maj� pozosta� tylko wpisy odno�nie usuni�tych produkt�w)

	-- wykorzystaj tSQLt.ApplyTrigger do na�o�enia triggera Storage.Trigger_WarehouseInventory_ProductQuantityHistory
	-- na tabel� Storage.WarehouseInventory

	-- Given
	-- When
	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END

GO

EXEC tSQLt.Run 'Storage_WarehouseInventoryTests.[test that it logs when product is deleted from inventory]'