USE tSQLtTraining_M3

IF OBJECT_ID('Storage_WarehouseInventoryTests.[test that it logs when product is added to inventory]') IS NOT NULL
DROP PROCEDURE Storage_WarehouseInventoryTests.[test that it logs when product is added to inventory]
GO

CREATE PROCEDURE Storage_WarehouseInventoryTests.[test that it logs when product is added to inventory]
AS
BEGIN
	-- Przetestuj czy odpowiednie wiersze s� dodawane do tabeli Storage.WarehouseInventoryHistory
	-- kiedy s� dodawane nowe wiersze do tabeli Storage.WarehouseInventory przez trigger
	-- wykorzystaj tSQLt.ApplyTrigger do na�o�enia triggera Storage.Trigger_WarehouseInventory_ProductQuantityHistory
	-- na tabel� Storage.WarehouseInventory

	-- Given
	-- When
	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END

GO

EXEC tSQLt.Run 'Storage_WarehouseInventoryTests.[test that it logs when product is added to inventory]'