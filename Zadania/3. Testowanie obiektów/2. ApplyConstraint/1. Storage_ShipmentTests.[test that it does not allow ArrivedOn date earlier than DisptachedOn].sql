USE tSQLtTraining_M3

IF OBJECT_ID('Storage_ShipmentTests.[test that it does not allow ArrivedOn date earlier than DisptachedOn]') IS NOT NULL
DROP PROCEDURE Storage_ShipmentTests.[test that it does not allow ArrivedOn date earlier than DisptachedOn]

GO

CREATE PROCEDURE Storage_ShipmentTests.[test that it does not allow ArrivedOn date earlier than DisptachedOn]
AS
BEGIN
	-- Przetestuj czy constraint CK_Shipment_ArrivedOn_LaterOrEqualTo_DispatchedOn
	-- pozawala na ustawienie kolmuny ArrivedOn wcze�niejszej niz DispatchedOn w tabeli Storage.Shipment

	-- HINT: Mo�esz sprawdzi� czy zostanie rzucony wyj�tek podczas wsadzania/modyfikacji danych zawieraj�cy nazw� constrainta
	-- tSQLt.ExpectException @ExpectedMessagePattern = '%NAZWA_CONSTRAINTA%'

	-- Given
	DECLARE @constraintName NVARCHAR(50) = 'CK_Shipment_ArrivedOn_LaterOrEqualTo_DispatchedOn'

	-- When
	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END

GO

EXEC tSQLt.Run 'Storage_ShipmentTests.[test that it does not allow ArrivedOn date earlier than DisptachedOn]'