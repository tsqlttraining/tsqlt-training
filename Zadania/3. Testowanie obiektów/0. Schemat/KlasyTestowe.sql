USE tSQLtTraining_M3

CREATE SCHEMA Storage_OutgoingWarehouseShipmentsTests
    AUTHORIZATION [dbo];

GO

EXECUTE sp_addextendedproperty @name = N'tSQLt.TestClass', @value = 1, @level0type = N'SCHEMA',
	@level0name = N'Storage_OutgoingWarehouseShipmentsTests';
GO

CREATE SCHEMA Storage_ProductsInShipmentTests
    AUTHORIZATION [dbo];

GO

EXECUTE sp_addextendedproperty @name = N'tSQLt.TestClass', @value = 1, @level0type = N'SCHEMA',
	@level0name = N'Storage_ProductsInShipmentTests';
GO

CREATE SCHEMA Storage_ShipAllOrdersTests
    AUTHORIZATION [dbo];

GO

EXECUTE sp_addextendedproperty @name = N'tSQLt.TestClass', @value = 1, @level0type = N'SCHEMA',
	@level0name = N'Storage_ShipAllOrdersTests';
GO

CREATE SCHEMA Storage_ShipmentTests
    AUTHORIZATION [dbo];

GO

EXECUTE sp_addextendedproperty @name = N'tSQLt.TestClass', @value = 1, @level0type = N'SCHEMA',
	@level0name = N'Storage_ShipmentTests';
GO

CREATE SCHEMA Storage_ShipOrderProductsTests
    AUTHORIZATION [dbo];

GO

EXECUTE sp_addextendedproperty @name = N'tSQLt.TestClass', @value = 1, @level0type = N'SCHEMA',
	@level0name = N'Storage_ShipOrderProductsTests';
GO

CREATE SCHEMA Storage_WarehouseInventoryTests
    AUTHORIZATION [dbo];

GO

EXECUTE sp_addextendedproperty @name = N'tSQLt.TestClass', @value = 1, @level0type = N'SCHEMA',
	@level0name = N'Storage_WarehouseInventoryTests';
GO

CREATE SCHEMA TestUtils
    AUTHORIZATION [dbo];
GO
