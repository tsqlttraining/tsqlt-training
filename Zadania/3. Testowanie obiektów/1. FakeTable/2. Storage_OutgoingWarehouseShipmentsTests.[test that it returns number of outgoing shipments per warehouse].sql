USE tSQLtTraining_M3

IF OBJECT_ID('Storage_OutgoingWarehouseShipmentsTests.[test that it returns number of outgoing shipments per warehouse]') IS NOT NULL
DROP PROCEDURE Storage_OutgoingWarehouseShipmentsTests.[test that it returns number of outgoing shipments per warehouse]
GO

CREATE PROCEDURE Storage_OutgoingWarehouseShipmentsTests.[test that it returns number of outgoing shipments per warehouse]
AS
BEGIN
	-- Given
	-- przygotuj dane tak aby test przechodzi�
	-- wykorzystaj tSQLt.FakeTable z opcj� zachowania Identity na tabeli Storage.Shipment 
	-- w celu unikni�cia r�cznego przypisywania ShipmentId
	
	-- When
	SELECT *
	INTO actual
	FROM Storage.OutgoingWarehouseShipments

	-- Then
	CREATE TABLE expected (
		WarehouseId INT,
		NumberOfShipments INT,
	)

	INSERT INTO expected
	VALUES (1, 2),
		(2, 3),
		(3, 0)

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END

GO

EXEC tSQLt.Run 'Storage_OutgoingWarehouseShipmentsTests.[test that it returns number of outgoing shipments per warehouse]'