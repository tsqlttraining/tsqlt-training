USE tSQLtTraining_M3

IF OBJECT_ID('Storage_ProductsInShipmentTests.[test that it returns only products in shipment]') IS NOT NULL
DROP PROCEDURE Storage_ProductsInShipmentTests.[test that it returns only products in shipment]
GO

CREATE PROCEDURE Storage_ProductsInShipmentTests.[test that it returns only products in shipment]
AS
BEGIN
	-- Given
	-- wstaw dane tak �eby test przechodzi�
	-- wykorzystaj tSQLt.FakeTable w celu unikni�cia wsadzania zb�dnych danych

	-- When
	SELECT *
	INTO actual
	FROM Storage.ProductsInShipment

	-- Then
	CREATE TABLE expected (
		ProductName NVARCHAR(60),
		Quantity INT,
		DispatchedOn DATETIME2,
	)

	INSERT INTO expected
	VALUES ('Paper towel', 160, '2017-11-01 14:10'),
		('Paper Towel', 50, '2016-10-30 09:35'),
		('Rubber duck', 42, '2016-11-02 11:32'),
		('Scissors', 30, '2016-11-03 12:45')

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END

GO

EXEC tSQLt.Run 'Storage_ProductsInShipmentTests.[test that it returns only products in shipment]'