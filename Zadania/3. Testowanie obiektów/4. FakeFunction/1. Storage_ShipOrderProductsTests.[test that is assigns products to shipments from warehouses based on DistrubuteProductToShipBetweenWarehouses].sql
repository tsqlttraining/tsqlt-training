USE tSQLtTraining_M3

IF OBJECT_ID('Storage_ShipOrderProductsTests.[test that is assigns products to shipments from warehouses based on DistrubuteProductToShipBetweenWarehouses]') IS NOT NULL
DROP PROCEDURE Storage_ShipOrderProductsTests.[test that is assigns products to shipments from warehouses based on DistrubuteProductToShipBetweenWarehouses]

IF OBJECT_ID('Storage_ShipOrderProductsTests.[Fake_ProductDistribution]') IS NOT NULL
DROP TABLE Storage_ShipOrderProductsTests.[Fake_ProductDistribution]

IF OBJECT_ID('Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses') IS NOT NULL
DROP FUNCTION Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses

GO

CREATE TABLE Storage_ShipOrderProductsTests.[Fake_ProductDistribution]
(
	[WarehouseId] INT,
	[Quantity] INT
)

GO

CREATE FUNCTION Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses
(
	@productId INT,
	@desiredQuantity INT,
	@excludedWarehouseId INT
)
RETURNS TABLE AS RETURN
(
	SELECT WarehouseId, Quantity AS QuantityToShip
	FROM Storage_ShipOrderProductsTests.Fake_ProductDistribution
)
GO

CREATE PROCEDURE Storage_ShipOrderProductsTests.[test that is assigns products to shipments from warehouses based on DistrubuteProductToShipBetweenWarehouses]
AS
BEGIN
	-- Given
	EXEC tSQLt.FakeTable 'Storage.Order'
	EXEC tSQLt.FakeTable 'Storage.OrderShipment'
	EXEC tSQLt.FakeTable 'Storage.Shipment', @Identity = 1
	EXEC tSQLt.FakeTable 'Storage.ShipmentProduct'
	
	DECLARE @orderId INT = 1
	DECLARE @productId INT = 1
	DECLARE @orderWarehouseId INT = 1

	INSERT INTO Storage.[Order] (OrderId, ProductId, DestinationWarehouseId, Quantity)
	VALUES (@orderId, @productId, @orderWarehouseId, 10)

	-- Zamie� funkcj� Storage.DistributeProductToShipBetweenWarehouses
	-- z funkcj� Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses 
	-- spraw by funkcja Storage.DistributeProductToShipBetweenWarehouses zwraca�a nast�puj�c� dystrybucj� produkt�w
	-- Magazyn o Id 2 - 5 produkt�w
	-- Magazyn o Id 3 - 3 produkty
	-- Magazyn o Id 4 - 2 produkty

	-- When
	EXEC Storage.ShipOrderProducts @orderId = @orderId

	-- Then
	SELECT s.SourceWarehouseId, s.DestinationWarehouseId, sp.ProductId, sp.Quantity
	INTO actual
	FROM Storage.ShipmentProduct AS sp
	JOIN Storage.Shipment AS s ON s.ShipmentId = sp.ShipmentId

	CREATE TABLE expected (
		SourceWarehouseId INT,
		Quantity INT,
		DestinationWarehouseId INT,
		ProductId INT
	)

	INSERT INTO expected
	VALUES (2, 5, @orderWarehouseId, @productId),
		(3, 3, @orderWarehouseId, @productId),
		(4, 2, @orderWarehouseId, @productId)

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END

GO 

EXEC tSQLt.Run 'Storage_ShipOrderProductsTests.[test that is assigns products to shipments from warehouses based on DistrubuteProductToShipBetweenWarehouses]'