USE tSQLtTraining_M3

IF OBJECT_ID('Storage_WarehouseInventoryTests.[test that it logs with current date]') IS NOT NULL
DROP PROCEDURE Storage_WarehouseInventoryTests.[test that it logs with current date]

IF OBJECT_ID('TestUtils.[Fake_GetSysDateTime]') IS NOT NULL
DROP FUNCTION TestUtils.[Fake_GetSysDateTime]

GO

CREATE FUNCTION TestUtils.[Fake_GetSysDateTime]()
RETURNS DATETIME2(7)
AS
BEGIN
	RETURN NULL
END

GO

CREATE PROCEDURE Storage_WarehouseInventoryTests.[test that it logs with current date]
AS
BEGIN
	-- Przetestuj czy wiersze dodawane do tabeli Storage.WarehouseInventoryHistory maj� odpowiedni� dat�
	-- trigger wykorzystuje funkcj� utils.GetSysDateTime() do pobrania aktualnej daty, poniewa� tSQLt nie jest w stanie podmienic funkcji systemowych (np. GETDATE(), SYSDATETIME())

	-- wykorzystaj tSQLt.ApplyTrigger do na�o�enia triggera Storage.Trigger_WarehouseInventory_ProductQuantityHistory
	-- na tabel� Storage.WarehouseInventory
	-- wykorzystaj tSQLt.FakeFuncction do podmienienia funkcji utils.GetSysDateTime na TestUtils.Fake_GetSysDateTime

	-- Given
	-- When
	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END

GO

EXEC tSQLt.Run 'Storage_WarehouseInventoryTests.[test that it logs with current date]'