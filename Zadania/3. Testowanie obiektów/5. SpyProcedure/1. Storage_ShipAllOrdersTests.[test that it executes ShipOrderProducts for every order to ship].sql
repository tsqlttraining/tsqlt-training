USE tSQLtTraining_M3

IF OBJECT_ID('Storage_ShipAllOrdersTests.[test that it executes ShipOrderProducts for every order to ship]') IS NOT NULL
DROP PROCEDURE Storage_ShipAllOrdersTests.[test that it executes ShipOrderProducts for every order to ship]
GO

CREATE PROCEDURE Storage_ShipAllOrdersTests.[test that it executes ShipOrderProducts for every order to ship]
AS
BEGIN
	-- Przetestuj czy prcedure Storage.ShipAllOrders wywo�uje procedure Storage.ShipOrderProducts
	-- dla wszystkich zam�wie� zwr�conych przez widok Storage.ProductsToShip

	-- U�yj tSQLt.FakeTable na widoku Storage.ProductsToShip by w �atwy spos�b podmieni� dane
	-- zwracane przez ten widok (b�dzie zachowywa� si� jak tabela)

	-- U�yj tSQLt.SpyProcedure do zamockowania procedury Storage.ShipOrderProducts
	-- Sprawd� czy procedura Storage.ShipOrderProducts zosta�a wywo�ana ze wszystkimi zam�wieniami
	-- zwr�conymi przez Storage.ProductsToShip
	-- Wykorzystaj stworz�n� przez tSQLt tabel� Storage.ShipOrderProducts_SpyProcedureLog

	-- Given
	-- When
	-- Then
	EXEC tSQLt.Fail 'Not implemented'
END

GO

EXEC tSQLt.Run 'Storage_ShipAllOrdersTests.[test that it executes ShipOrderProducts for every order to ship]'