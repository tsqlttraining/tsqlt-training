﻿--Uzyj tylko na bazie tSQLtTraining_DestinationDB
USE tSQLtTraining_DestinationDB

IF OBJECT_ID('SSISTests.[test if Warehouse data is migrated correctly]') IS NOT NULL 
DROP PROCEDURE SSISTests.[test if Warehouse data is migrated correctly]
GO 

CREATE PROCEDURE SSISTests.[test if Warehouse data is migrated correctly] 
AS
BEGIN
    -- Given
    IF OBJECT_ID('actual') IS NOT NULL DROP TABLE actual;
	IF OBJECT_ID('expected') IS NOT NULL DROP TABLE expected;

    -- Nie mozemy fake'owac bo SSIS nie moze sie dostać do zalokowanej tabeli z innej transakcji
    -- EXEC tSQLtTraining_SourceDB.tSQLt.FakeTable 'tSQLtTraining_SourceDB.Storage.Warehouse'
    EXEC tSQLt.FakeTable 'tSQLtTraining_DestinationDB.Storage.Warehouse', @Identity = 1 

    -- wiec musimy wstawic dane bezposrednio do interesujacej nas tabeli
    SET IDENTITY_INSERT tSQLtTraining_SourceDB.Storage.Warehouse ON 

    INSERT INTO tSQLtTraining_SourceDB.Storage.Warehouse (
	    WarehouseId
	    ,[Name]
	    ,Country
	    ,City
	    ,AddressLine1
	    ,AddressLine2
	    )
    VALUES (
	    1
	    ,'Primary Warehouse'
	    ,'Poland'
	    ,'Warszawa'
	    ,'Wojewódzka'
	    ,'3a'
	    )
	    ,(
	    2
	    ,'Secondary Warehouse'
	    ,'Poland'
	    ,'Bytom'
	    ,'Zielona'
	    ,'15'
	    );
    
    SET IDENTITY_INSERT tSQLtTraining_SourceDB.Storage.Warehouse OFF

    -- When
    -- i tak nie zadziala dla SSIS bo dane sa kopiowane w osobnej transakcji
    EXEC Storage.MigrateByRunningSSISPackage;

    SELECT * 
    INTO expected
    FROM tSQLtTraining_SourceDB.Storage.WarehousesToMigrate;

    SELECT * 
    INTO actual
    FROM Storage.Warehouse;

    -- DEBUG only :)
    --select * from expected;
    --select * from actual;

    -- Then
    EXEC tSQLt.AssertEqualsTable 'expected'
		,'actual'
END
GO 

EXEC tSQLt.Run 'SSISTests.[test if Warehouse data is migrated correctly]'