﻿--Uzyj tylko na bazie tSQLtTraining_DestinationDB
USE tSQLtTraining_DestinationDB

IF OBJECT_ID('SSISTests.[test if Warehouse data is migrated correctly]') IS NOT NULL 
DROP PROCEDURE SSISTests.[test if Warehouse data is migrated correctly]
GO 

CREATE PROCEDURE SSISTests.[test if Warehouse data is migrated correctly] 
AS
BEGIN
    -- Given
    IF OBJECT_ID('actual') IS NOT NULL DROP TABLE actual;
	IF OBJECT_ID('expected') IS NOT NULL DROP TABLE expected;

    EXEC tSQLtTraining_SourceDB.tSQLt.FakeTable 'tSQLtTraining_SourceDB.Storage.Warehouse' 
    EXEC tSQLt.FakeTable 'tSQLtTraining_DestinationDB.Storage.Warehouse', @Identity = 1 

    INSERT INTO tSQLtTraining_SourceDB.Storage.Warehouse (
	    WarehouseId
	    ,[Name]
	    ,Country
	    ,City
	    ,AddressLine1
	    ,AddressLine2
	    )
    VALUES (
	    1
	    ,'Primary Warehouse'
	    ,'Poland'
	    ,'Warszawa'
	    ,'Wojewódzka'
	    ,'3a'
	    )
	    ,(
	    2
	    ,'Secondary Warehouse'
	    ,'Poland'
	    ,'Bytom'
	    ,'Zielona'
	    ,'15'
	    );

    -- When
    EXEC Storage.MigrateByCopyingDataManually;

    SELECT * 
    INTO expected
    FROM tSQLtTraining_SourceDB.Storage.WarehousesToMigrate;

    SELECT * 
    INTO actual
    FROM Storage.Warehouse;

    -- DEBUG only :)
    --select * from expected;
    --select * from actual;

    -- Then
    EXEC tSQLt.AssertEqualsTable 'expected'
		,'actual'
END
GO 

EXEC tSQLt.Run 'SSISTests.[test if Warehouse data is migrated correctly]'