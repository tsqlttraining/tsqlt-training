﻿--Uzyj tylko na bazie tSQLtTraining_DestinationDB_NT 
USE tSQLtTraining_DestinationDB_NT

IF OBJECT_ID('SSISTests.[test if Warehouse data is migrated correctly]') IS NOT NULL 
DROP PROCEDURE SSISTests.[test if Warehouse data is migrated correctly]
GO 

CREATE PROCEDURE SSISTests.[test if Warehouse data is migrated correctly] 
AS
BEGIN
    -- Given
    IF OBJECT_ID('actual') IS NOT NULL DROP TABLE actual;
	IF OBJECT_ID('expected') IS NOT NULL DROP TABLE expected;

    -- Nie ma juz sensu fake'owac bo nie wycofa zmiany
    -- wiec musimy wstawic dane bezposrednio do interesujacej nas tabeli
    SET IDENTITY_INSERT tSQLtTraining_SourceDB_NT.Storage.Warehouse ON 

    INSERT INTO tSQLtTraining_SourceDB_NT.Storage.Warehouse (
	    WarehouseId
	    ,[Name]
	    ,Country
	    ,City
	    ,AddressLine1
	    ,AddressLine2
	    )
    VALUES (
	    1
	    ,'Primary Warehouse'
	    ,'Poland'
	    ,'Warszawa'
	    ,'Wojewódzka'
	    ,'3a'
	    )
	    ,(
	    2
	    ,'Secondary Warehouse'
	    ,'Poland'
	    ,'Bytom'
	    ,'Zielona'
	    ,'15'
	    );
    
    SET IDENTITY_INSERT tSQLtTraining_SourceDB_NT.Storage.Warehouse OFF

    -- When
    EXEC Storage.MigrateByRunningSSISPackage;

    SELECT * 
    INTO expected
    FROM tSQLtTraining_SourceDB_NT.Storage.WarehousesToMigrate;

    SELECT * 
    INTO actual
    FROM Storage.Warehouse;

    -- DEBUG only :)
    --select * from expected;
    --select * from actual;

    -- Then
    EXEC tSQLt.AssertEqualsTable 'expected'
		,'actual'
END
GO 

IF OBJECT_ID('SSISTests.[test if warehouse data is empty when there was no source data]') IS NOT NULL 
DROP PROCEDURE SSISTests.[test if warehouse data is empty when there was no source data]
GO 

CREATE PROCEDURE SSISTests.[test if warehouse data is empty when there was no source data] 
AS
BEGIN
    -- Given

    -- When
    EXEC Storage.MigrateByRunningSSISPackage;

    -- Then
    EXEC tSQLt.AssertEmptyTable 'Storage.Warehouse'

END
GO 

IF OBJECT_ID('SSISTests.[SetUp]') IS NOT NULL 
DROP PROCEDURE SSISTests.[SetUp]
GO 

CREATE PROCEDURE SSISTests.SetUp
AS
BEGIN
    DELETE FROM tSQLtTraining_SourceDB_NT.Storage.WarehousesToMigrate;
    DELETE FROM tSQLtTraining_DestinationDB_NT.Storage.Warehouse;
END
GO

EXEC tSQLt.RunAll

