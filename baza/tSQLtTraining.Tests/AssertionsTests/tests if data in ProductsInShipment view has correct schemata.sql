﻿
CREATE PROCEDURE AssertionsTests.[tests if data in ProductsInShipment view has correct schemata]
AS
BEGIN    
	--Assemble
	DECLARE @actual nvarchar(max) = N'SELECT * FROM Storage.ProductsInShipment';
	DECLARE @expected nvarchar(max) = N'
	CREATE TABLE expected (
		ProductName NVARCHAR(60) NOT NULL,
		Quantity INT NOT NULL,
		DispatchedOn DATETIME2
	)
	SELECT * FROM expected
	'

	--Act
	--Assert
	EXEC tSQLt.AssertResultSetsHaveSameMetaData @expected
		,@actual
END