﻿
CREATE PROCEDURE AssertionsTests.[tests if ProductsInShipment has correct schemata]
AS
BEGIN
	IF OBJECT_ID('actual') IS NOT NULL
		DROP TABLE actual;
	IF OBJECT_ID('expected') IS NOT NULL
		DROP TABLE expected;
    
	--Act

	 SELECT *
	 INTO actual
	 FROM Storage.ProductsInShipment

	--Assert

	CREATE TABLE expected (
		ProductName NVARCHAR(60) NOT NULL,
		Quantity INT NOT NULL,
		DispatchedOn DATETIME2
	)
	
	EXEC tSQLt.AssertEqualsTableSchema 'expected'
		,'actual'
END