﻿
CREATE PROCEDURE AssertionsTests.[test exception is raised when adding too long Product name]
AS
BEGIN
	--Assemble
	EXEC tSQLt.ExpectException @ExpectedMessagePattern = 'Cannot create product with name longer than % characters'
	DECLARE @productName NVARCHAR(100) = REPLICATE('Product Name', 10)

	--Act
	EXEC Production.AddNewProduct @ProductName

	--Assert

END