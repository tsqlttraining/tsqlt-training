﻿
CREATE PROCEDURE AssertionsTests.[tests if ProductsInShipment returns correct data]
AS
BEGIN
	IF OBJECT_ID('actual') IS NOT NULL
		DROP TABLE actual;
	IF OBJECT_ID('expected') IS NOT NULL
		DROP TABLE expected;

	--Assemble
	SET IDENTITY_INSERT Production.Product ON

	INSERT INTO Production.Product (ProductId, [Name])
	VALUES (1, 'Paper towel'),
		(2, 'Rubber duck'),
		(3, 'Scissors'),
		(4, 'Not Shipped Product')

	SET IDENTITY_INSERT Production.Product OFF

	SET IDENTITY_INSERT Storage.Warehouse ON

	INSERT INTO Storage.Warehouse (WarehouseId, [Name], AddressLine1, AddressLine2, City, Country)
	VALUES (1,'East London 1', 'adas', 'asfa', 'goanq', 'afji'), 
		(2,'East London 2', 'adas', 'asfa', 'goanq', 'afji')

	SET IDENTITY_INSERT Storage.Warehouse OFF

	SET IDENTITY_INSERT Storage.Shipment ON

	INSERT INTO Storage.Shipment (ShipmentId, DispatchedOn, ArrivedOn, SourceWarehouseId, DestinationWarehouseId)
	VALUES (1,'2017-11-01 14:10',null, 1, 2),
		(2,'2016-10-30 09:35',null, 1, 2),
		(3,'2016-11-02 11:32',null, 1, 2),
		(4,'2016-11-03 12:45',null, 1, 2),
		(5,'2016-11-03 12:45','2017-11-03 12:45', 1, 2)

	SET IDENTITY_INSERT Storage.Shipment OFF

	INSERT INTO Storage.ShipmentProduct (ShipmentId, ProductId, Quantity)
	VALUES (1,1, 160),
		(2,1,50),
		(3,2,42),
		(4,3,30),
		(5,1,160)
	

	--Act
	SELECT *
	INTO actual
	FROM Storage.ProductsInShipment

	--Assert
	CREATE TABLE expected (
		ProductName NVARCHAR(60),
		Quantity INT,
		DispatchedOn DATETIME2,
	)

	INSERT INTO expected
	VALUES ('Paper towel', 160, '2017-11-01 14:10'),
		('Paper Towel', 50, '2016-10-30 09:35'),
		('Rubber duck', 42, '2016-11-02 11:32'),
		('Scissors', 30, '2016-11-03 12:45')

	EXEC tSQLt.AssertEqualsTable 'expected'
		,'actual'
END