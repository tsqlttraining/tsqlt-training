﻿
CREATE PROCEDURE AssertionsTests.[tests if utils.Min returns parameter with lower value]
AS
BEGIN
	--Assemble
	DECLARE @smaller INT = 1
	DECLARE @larger INT = 5

	--Act
	DECLARE @result INT

	SELECT @result = utils.[Min](@smaller, @larger)

	--Assert	
	EXEC tSQLt.AssertNotEquals @larger, @result
END