﻿using tSQLt.Client.Net;
using System.Configuration;
using System;

namespace tSQLtTraining.NUnit.Tests
{
    public class BaseTSQLtTest
    {
        private static string ConnectionString = ConfigurationManager.ConnectionStrings["TestDatabaseConnection"].ConnectionString;
        private static TimeSpan TestTimeout = TimeSpan.FromMinutes(1);
        protected tSQLtTestRunner Runner { get; private set; }

        public BaseTSQLtTest()
        {
            Runner = new tSQLtTestRunner(ConnectionString, (int)TestTimeout.TotalMilliseconds);
        }
    }
}
