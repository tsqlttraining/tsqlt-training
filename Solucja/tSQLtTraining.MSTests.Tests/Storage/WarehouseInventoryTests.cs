﻿using tSQLtTraining.NUnit.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace ExampleProject
{
    [TestClass]
    public class WarehouseInventoryTests : BaseTSQLtTest
    {
        private const string TestClassName = "Storage_WarehouseInventoryTests";

        [TestMethod]
        public void TestThatItLogsWhenProductIsAddedToInventory()
        {
            var result = Runner.Run(TestClassName, "test that it logs when product is added to inventory");

            Assert.IsTrue(result.Passed(), result.FailureMessages());
        }

        [TestMethod]
        public void TestThatItLogsWhenProductIsDeletedFromInventory()
        {
            var result = Runner.Run(TestClassName, "test that it logs when product is deleted from inventory");

            Assert.IsTrue(result.Passed(), result.FailureMessages());
        }

        [TestMethod]
        public void TestThatItLogsWithCurrentDate()
        {
            var result = Runner.Run(TestClassName, "test that it logs with current date");

            Assert.IsTrue(result.Passed(), result.FailureMessages());
        }
    }
}