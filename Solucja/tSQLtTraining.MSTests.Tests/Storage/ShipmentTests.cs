﻿using tSQLtTraining.NUnit.Tests;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace tSQLtTraining.MSTests.Tests.Storage
{
    [TestClass]
    public class ShipmentTests : BaseTSQLtTest
    {
        private const string TestClassName = "Storage_ShipmentTests";

        [TestMethod]
        public void ItShouldPassTests()
        {
            var result = Runner.RunClass(TestClassName);

            Assert.IsTrue(result.Passed(), result.FailureMessages());
        }
    }
}
