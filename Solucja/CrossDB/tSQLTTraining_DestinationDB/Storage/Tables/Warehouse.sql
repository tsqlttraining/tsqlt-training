﻿CREATE TABLE [Storage].[Warehouse] (
	[WarehouseId] INT           IDENTITY (1, 1) NOT NULL,
	[Name]        NVARCHAR (30) NOT NULL,
	[Country]     NVARCHAR (30) NOT NULL,
	[FullAddress] NVARCHAR (200) NOT NULL,
	PRIMARY KEY CLUSTERED ([WarehouseId] ASC)
);

