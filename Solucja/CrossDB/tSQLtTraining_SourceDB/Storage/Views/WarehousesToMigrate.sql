﻿CREATE VIEW [Storage].[WarehousesToMigrate]
AS
SELECT WarehouseId
	,[Name]
	,Country
	,City + ', ' + AddressLine1 + ' ' + AddressLine2 AS FullAddress
FROM [Storage].[Warehouse]
