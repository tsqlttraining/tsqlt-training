﻿CREATE TABLE [Storage].[Warehouse] (
    [WarehouseId]  INT           IDENTITY (1, 1) NOT NULL,
    [Name]         NVARCHAR (30) NOT NULL,
    [AddressLine1] NVARCHAR (60) NOT NULL,
    [AddressLine2] NVARCHAR (60) NOT NULL,
    [City]         NVARCHAR (30) NOT NULL,
    [Country]      NVARCHAR (30) NOT NULL,
    PRIMARY KEY CLUSTERED ([WarehouseId] ASC)
);

