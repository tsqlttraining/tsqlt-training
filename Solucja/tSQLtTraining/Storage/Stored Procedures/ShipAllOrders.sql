﻿CREATE PROCEDURE Storage.[ShipAllOrders]
AS
BEGIN
	DECLARE @transactionName VARCHAR(32) = REPLACE((CAST(NEWID() AS VARCHAR(36))),'-','')

	BEGIN TRY
		BEGIN TRANSACTION
		SAVE TRAN @transactionName;

		SELECT ROW_NUMBER() OVER(ORDER BY o.OrderId) AS RowNumber,
			o.OrderId
		INTO #ordersToShip
		FROM Storage.ProductsToShip AS o

		DECLARE @i INT

		SELECT @i = MIN(RowNumber)
		FROM #ordersToShip

		WHILE EXISTS (SELECT 1 FROM #ordersToShip)
		BEGIN
			DECLARE @orderId INT

			SELECT @orderId = OrderId
			FROM #ordersToShip
			WHERE RowNumber = @i

			EXECUTE Storage.ShipOrderProducts @orderId = @orderId

			DELETE FROM #ordersToShip
			WHERE RowNumber = @i

			SET @i = @i + 1
		END


		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION @transactionName;
		COMMIT TRANSACTION;
		THROW
	END CATCH
END