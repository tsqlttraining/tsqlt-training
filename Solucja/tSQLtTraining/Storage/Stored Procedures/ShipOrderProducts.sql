﻿CREATE PROCEDURE Storage.ShipOrderProducts
	@orderId INT
AS
BEGIN
	DECLARE @transactionName VARCHAR(32) = REPLACE((CAST(NEWID() AS VARCHAR(36))),'-','')

	BEGIN TRY
		BEGIN TRANSACTION
		SAVE TRAN @transactionName;

		DECLARE @exists BIT = 0, @productId INT, @quantityToShip INT, @destinationWarehouseId INT

		SELECT
			@exists = 1,
			@orderId = ps.OrderId,
			@productId = ps.ProductId,
			@quantityToShip = ps.QuantityToShip,
			@destinationWarehouseId = ps.DestinationWarehouseId
		FROM Storage.ProductsToShip AS ps
		WHERE OrderId = @orderId

		IF @exists <> 1
		BEGIN;
			THROW 51000, 'Cannot ship not existing product!', 1;
		END

		SELECT d.WarehouseId, d.QuantityToShip
		INTO #productDistribution
		FROM Storage.DistributeProductToShipBetweenWarehouses(@productId, @quantityToShip, @destinationWarehouseId) AS d

		DECLARE @shippedQuantity INT
		SELECT @shippedQuantity = SUM(QuantityToShip) FROM #productDistribution

		IF (@shippedQuantity > 0)
		BEGIN
			-- Reduce quantities stored in warehouses by what will be shipped
			UPDATE wi
			SET Quantity = wi.Quantity - d.QuantityToShip
			FROM Storage.WarehouseInventory AS wi
			JOIN #productDistribution AS d ON wi.WarehouseId = d.WarehouseId

			DECLARE @shipmentsToDestinationWarehouse TABLE (
				ShipmentId INT NOT NULL,
				SourceWarehouseId INT NOT NULL
			)

			-- Select existing shipments not yet dispatched 			
			INSERT INTO @shipmentsToDestinationWarehouse (ShipmentId, SourceWarehouseId)
			SELECT MIN(s.ShipmentId), s.SourceWarehouseId
			FROM Storage.Shipment AS s
			JOIN #productDistribution AS d ON s.SourceWarehouseId = d.WarehouseId
			WHERE d.QuantityToShip > 0
			AND s.DestinationWarehouseId = @destinationWarehouseId
			AND s.DispatchedOn IS NULL
			GROUP BY s.SourceWarehouseId

			-- Add not existing shipments that will be dispatched
			INSERT INTO Storage.Shipment (SourceWarehouseId, DestinationWarehouseId)
			OUTPUT INSERTED.ShipmentId, INSERTED.SourceWarehouseId INTO @shipmentsToDestinationWarehouse
			SELECT d.WarehouseId, @destinationWarehouseId
			FROM #productDistribution AS d
			WHERE QuantityToShip > 0
			AND NOT EXISTS (
				SELECT 1
				FROM Storage.Shipment AS s
				WHERE s.SourceWarehouseId = d.WarehouseId
				AND s.DestinationWarehouseId = @destinationWarehouseId
				AND s.DispatchedOn IS NULL
			)

			INSERT INTO Storage.OrderShipment (OrderId, ShipmentId)
			SELECT @orderId, ShipmentId
			FROM @shipmentsToDestinationWarehouse

			-- Increase quantity of existing ShipemntProducts
			UPDATE sp
			SET Quantity = sp.Quantity + d.QuantityToShip
			FROM Storage.ShipmentProduct AS sp
			JOIN @shipmentsToDestinationWarehouse AS s ON s.ShipmentId = sp.ShipmentId
			JOIN #productDistribution AS d ON d.WarehouseId = s.SourceWarehouseId
			WHERE sp.ProductId = @productId

			-- Add entries to ShipemntProducts when they not exist
			INSERT INTO Storage.ShipmentProduct (ShipmentId, ProductId, Quantity)
			SELECT s.ShipmentId, @productId, d.QuantityToShip
			FROM @shipmentsToDestinationWarehouse s
			JOIN #productDistribution AS d ON d.WarehouseId = s.SourceWarehouseId
			WHERE NOT EXISTS (
				SELECT 1
				FROM Storage.Shipment AS s2
				JOIN Storage.ShipmentProduct AS sp2 ON sp2.ShipmentId = s2.ShipmentId
				WHERE sp2.ProductId = @productId
				AND s2.ShipmentId = s.ShipmentId
			) AND d.QuantityToShip > 0
		END

		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		ROLLBACK TRANSACTION @transactionName;
		COMMIT TRANSACTION;

		THROW
	END CATCH
END