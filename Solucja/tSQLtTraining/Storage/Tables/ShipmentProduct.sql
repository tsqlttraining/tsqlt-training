﻿CREATE TABLE Storage.[ShipmentProduct]
(
	[ShipmentId] INT NOT NULL,
	[ProductId] INT NOT NULL,
	[Quantity] INT NOT NULL, 
    
	CONSTRAINT [FK_ShipmentProduct_Shipment_ShipmentId_ShipmentId] FOREIGN KEY (ShipmentId) REFERENCES Storage.Shipment(ShipmentId),
	CONSTRAINT [FK_ShipmentProduct_Product_ProductId_ProductId] FOREIGN KEY (ProductId) REFERENCES Production.Product(ProductId), 
    CONSTRAINT [CK_ShipmentProduct_Quantity_GreaterThanZero] CHECK (Quantity > 0),
	

)
