﻿CREATE TABLE Storage.[WarehouseInventoryHistory]
(
	[WarehouseId] INT NOT NULL,
	[ProductId] INT NOT NULL,
	[PreviousQuantity] INT,
	[NewQuantity] INT,
	[ChangedOn] DATETIME2 NOT NULL
)
