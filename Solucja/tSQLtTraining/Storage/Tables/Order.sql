﻿CREATE TABLE Storage.[Order]
(
	[OrderId] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[ProductId] INT NOT NULL,
	[Quantity] INT NOT NULL,
	[DestinationWarehouseId] INT NOT NULL,
	[CreatedOn] DATETIME2 NOT NULL,
	[CompletedOn] DATETIME2, 

    CONSTRAINT [FK_Order_Product_ProductId_ProductId] FOREIGN KEY (ProductId) REFERENCES Production.Product(ProductId),
	CONSTRAINT [FK_Order_Warehouse_DestinationWarehouseId_WarehouseId] FOREIGN KEY (DestinationWarehouseId) REFERENCES Storage.Warehouse(WarehouseId),
	CONSTRAINT [CK_Order_Quantity_IsGreaterThanZero] CHECK (Quantity > 0),
	CONSTRAINT [CK_Order_CreatedOn_IsBeforeOrEqualThan_CompletedOn] CHECK (CreatedOn <= CompletedOn)
	
)
