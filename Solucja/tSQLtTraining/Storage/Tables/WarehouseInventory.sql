﻿CREATE TABLE Storage.[WarehouseInventory]
(
	[WarehouseId] INT NOT NULL,
	[ProductId] INT NOT NULL,
	[Bay] NVARCHAR(30) NOT NULL,
	[Shelf] NVARCHAR(30) NOT NULL,
	[Quantity] INT NOT NULL,
)

GO

CREATE TRIGGER [Storage].[Trigger_WarehouseInventory_ProductQuantityHistory] ON [Storage].[WarehouseInventory]
AFTER DELETE, INSERT, UPDATE
AS
BEGIN
	SET NoCount ON

	DECLARE @changeDate DATETIME2 = utils.GetSysDateTime() -- SYSDATETIME()
	
	INSERT INTO Storage.WarehouseInventoryHistory (WarehouseId, ProductId, PreviousQuantity, NewQuantity, ChangedOn)
	SELECT ISNULL(old.WarehouseId, new.WarehouseId), ISNULL(old.ProductId, new.ProductId), old.Quantity, new.Quantity, @changeDate
	FROM DELETED AS old
	FULL OUTER JOIN INSERTED AS new ON new.WarehouseId = old.WarehouseId AND new.ProductId = old.ProductId
	WHERE old.Quantity IS NULL
	OR new.Quantity IS NULL
	OR old.Quantity <> new.Quantity
END