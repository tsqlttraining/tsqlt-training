﻿CREATE TABLE Storage.[OrderShipment]
(
	OrderId INT NOT NULL,
	ShipmentId INT NOT NULL, 
    
	CONSTRAINT [PK_OrderShipment] PRIMARY KEY (OrderId, ShipmentId),
    CONSTRAINT [FK_OrderShipment_Order_OrderId_OrderId] FOREIGN KEY (OrderId) REFERENCES Storage.[Order](OrderId), 
    CONSTRAINT [FK_OrderShipment_Shipment_ShipmentId_ShipmentId] FOREIGN KEY (ShipmentId) REFERENCES Storage.Shipment(ShipmentId)
)
