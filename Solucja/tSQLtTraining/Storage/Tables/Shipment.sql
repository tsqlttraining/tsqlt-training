﻿CREATE TABLE Storage.[Shipment]
(
	[ShipmentId] INT NOT NULL PRIMARY KEY IDENTITY(1,1),
	[DispatchedOn] DATETIME2,
	[ArrivedOn] DATETIME2,
	[SourceWarehouseId] INT NOT NULL,
	[DestinationWarehouseId] INT NOT NULL,

	CONSTRAINT [FK_Shipment_Warehouse_SourceWarehouseId_WarehouseId] FOREIGN KEY ([SourceWarehouseId]) REFERENCES [Storage].[Warehouse]([WarehouseId]),
	CONSTRAINT [FK_Shipment_Warehouse_DestinationWarehouseId_WarehouseId] FOREIGN KEY ([DestinationWarehouseId]) REFERENCES [Storage].[Warehouse]([WarehouseId]), 
    CONSTRAINT [CK_Shipment_SourceWarehouseId_DestinationWarehouseId_Different] CHECK (SourceWarehouseId <> DestinationWarehouseId), 
    CONSTRAINT [CK_Shipment_ArrivedOn_LaterOrEqualTo_DispatchedOn] CHECK (ArrivedOn >= DispatchedOn),
	CONSTRAINT [CK_Shipment_ArrivedOn_CanOnlyHaveValueWhen_DispatchedOn_HasValue] CHECK (NOT (ArrivedOn IS NULL AND DispatchedOn IS NULL)),
)

