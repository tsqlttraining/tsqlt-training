﻿CREATE FUNCTION Storage.DistributeProductToShipBetweenWarehouses
(
	@productId INT,
	@desiredQuantity INT,
	@excludedWarehouseId INT
)
RETURNS TABLE AS RETURN
(
	WITH WarehouseProducts AS (
		SELECT ROW_NUMBER() OVER (ORDER BY w.WarehouseId ASC) AS RowNumber, w.WarehouseId, wi.Quantity
		FROM Storage.Warehouse AS w
		JOIN Storage.WarehouseInventory AS wi ON wi.WarehouseId = w.WarehouseId
		WHERE w.WarehouseId <> @excludedWarehouseId
		AND wi.ProductId = @productId
		AND wi.Quantity > 0
	), warehouseProductShipments AS (
		SELECT
			wp.RowNumber,
			wp.WarehouseId,
			utils.[Min](@desiredQuantity, wp.Quantity) AS QuantityToShip,
			utils.[Max](0, @desiredQuantity - wp.Quantity) AS QuantityLeftToDistribute
		FROM WarehouseProducts AS wp
		WHERE wp.RowNumber = 1

		UNION ALL

		SELECT
			wp.RowNumber,
			wp.WarehouseId,
			utils.[Min](wps.QuantityLeftToDistribute, wp.Quantity),
			utils.[Max](0, wps.QuantityLeftToDistribute - wp.Quantity)
		FROM WarehouseProducts AS wp
		JOIN warehouseProductShipments AS wps ON wps.RowNumber = wp.RowNumber - 1
	)
	SELECT WarehouseId, QuantityToShip
	FROM warehouseProductShipments
	WHERE QuantityToShip > 0
)
