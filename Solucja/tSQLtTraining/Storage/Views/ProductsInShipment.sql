﻿CREATE VIEW Storage.[ProductsInShipment]
AS
SELECT p.[Name] AS ProductName
	,sp.Quantity
	,s.DispatchedOn
FROM Storage.ShipmentProduct AS sp
JOIN Storage.Shipment AS s ON s.ShipmentId = sp.ShipmentId
JOIN Production.Product AS p ON p.ProductId = sp.ProductId
WHERE s.ArrivedOn IS NULL