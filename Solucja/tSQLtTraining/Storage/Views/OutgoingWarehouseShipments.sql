﻿CREATE VIEW Storage.[OutgoingWarehouseShipments]
AS
SELECT w.WarehouseId, COUNT(s.ShipmentId) AS NumberOfShipments
FROM Storage.Warehouse w
LEFT JOIN Storage.Shipment s ON s.SourceWarehouseId = w.WarehouseId
WHERE s.ArrivedOn IS NULL
GROUP BY w.WarehouseId