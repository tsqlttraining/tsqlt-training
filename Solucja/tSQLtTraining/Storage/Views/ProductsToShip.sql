﻿CREATE VIEW Storage.ProductsToShip
AS
	WITH ProductsToShip AS (
		SELECT o.OrderId, o.ProductId, o.DestinationWarehouseId, o.Quantity - ISNULL(sip.AlreadyShippedQuantity, 0) AS QuantityToShip
		FROM Storage.[Order] AS o
		LEFT JOIN (
			SELECT os.OrderId, sp.ProductId, SUM(sp.Quantity) AS AlreadyShippedQuantity
			FROM Storage.OrderShipment AS os
			JOIN Storage.Shipment AS s ON s.ShipmentId = os.ShipmentId
			JOIN Storage.ShipmentProduct AS sp ON sp.ShipmentId = s.ShipmentId
			GROUP BY os.OrderId, sp.ProductId
		) AS sip ON sip.OrderId = o.OrderId AND sip.ProductId = o.ProductId
		WHERE o.CompletedOn IS NULL
	)
	SELECT OrderId, ProductId, DestinationWarehouseId, QuantityToShip
	FROM ProductsToShip
	WHERE QuantityToShip > 0
