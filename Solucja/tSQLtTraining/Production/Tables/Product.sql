﻿CREATE TABLE [Production].[Product]
(
	[ProductId] INT NOT NULL PRIMARY KEY IDENTITY (1,1),
	[Name] NVARCHAR(60) NOT NULL,
)
