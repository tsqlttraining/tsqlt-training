﻿CREATE  PROCEDURE Production.AddNewProduct 
	@productName nvarchar(max)
AS
BEGIN
	DECLARE @exists BIT = 0
	DECLARE @message nvarchar(max)

	SELECT @exists = 1
	FROM Production.Product
	WHERE [Name] = @productName

	IF @exists = 1
	BEGIN;
	   SET @message = 'Cannot create same product (' + @productName +') twice!';
	   THROW 51000, @message, 1;
	END

	IF len(@productName) > 60
	BEGIN;
	   SET @message = 'Cannot create product with name longer than 60 characters';
	   THROW 51000, @message, 2;
	END

	INSERT Production.Product ([Name]) VALUES (@productName)

	RETURN 0
END