﻿CREATE FUNCTION utils.[Min] (@a INT, @b INT)
RETURNS INT
AS
BEGIN
	RETURN IIF(@a > @b, @b, @a)
END
