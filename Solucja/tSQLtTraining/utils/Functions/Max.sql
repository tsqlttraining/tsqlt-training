﻿CREATE FUNCTION utils.[Max] (@a INT, @b INT)
RETURNS INT
AS
BEGIN
	RETURN IIF(@a > @b, @a, @b)
END
