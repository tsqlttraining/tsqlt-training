****Organizacja nowego �rodowiska do test�w bazodanowych z u�yciem tSQLt****

**Stworzenie projektu z baz� danych, kt�r� chcemy testowa�**

Najcz�sciej ten krok b�dziemy mie� ju� za sob� bo raczej b�dziemy dysponowa� baz� danych przygotowan� przez zesp� developerski i projektem bazy utworzonym przy pomocy SSDT. Za��my jednak, �e tak nie jest i stw�rzmy sobie nowy projekt bazy danych wykorzystuj�c baz� istniej�c� na SQL Server. 

Do tego celu mo�esz: 
- utworzy� now� baz� danych nazwijmy j� "MyDB" na serwerze i doda� jak�� najprostsz� tabel� lub dwie, tak aby�my mogli wykorzysta� t� baz� do test�w, 
- skorzysta� z backupu bazy tSQLtTraining znajduj�cej si� na dysku i wczyta� kopi� zapasow� za pomoc� SSMS'a mo�esz zmieni� jej nazw� na "MyDB"
- wykorzysta� t� solucj� i wykona� "Publish" bazy danych tSQLtTraining nadaj�� jej nazw� "MyDB"

1. W Visual Studio dodaj do folderu SettingUpNewDatabaseProject nowy projekt typu "SQL Server Database Project" i nazwij go "MyDB" tak jak baz� danych. 
2. Po utworzeniu projektu musimy zaimportowa� baz� danych z SQL Server'a do projektu bazodanowego. W tym celu kliknij na projekcie "MyDB" prawym przyciskiem i wybierz opcj� "Import -> Database..."
3. W wy�wietlonym oknie wybieramy po��czenie do bazy danych na kt�rej zainstalowali�my baz� "MyDB" i  klikamy "Start", wy�wietlone zostanie okno z post�pem importu. Po jego pomy�lnym zako�czeniu klikamy "Finish" i od teraz mamy projekt bazodanowy, kt�ry mo�emy �atwo publikowa� na SQL Server.

**Stworzenie projektu z baz� testow�**
1. Utw�rzmy w folderze SettingUpNewDatabaseProject drugi projekt bazodanowy, kt�ry b�dzie zawiera� kopi� naszej bazy produkcyjnej oraz zainstalowany framework tSQLt. Nazwijmy ten projekt "MyDB.Tests"
2. Jak tylko nasz projekt b�dzie gotowy dodajmy do niego referencj� do naszego projektu bazy produkcyjnej. W tym celu rozwijamy w "Solution Explorer" projekt "MyDB.Tests" i klikamy prawym przyciskiem na "References", a nast�pnie wybieramy opcj� "Add Database Reference...". 
3. W wy�wietlonym oknie w sekcji "Database Reference" wybieramy "Database projects in the current solution" i nast�pnie wskazujemy baz� danych "MyDB". W sekcji "Database Location" wybieramy "Same database" i klikamy "OK" - referencja do naszej bazy zosta�a dodana. 
4. W dalszej cz�ci potrzebna b�dzie nam baza z frameworkiem tSQLt. Je�li ju� tak� mamy to wystarczy j� wykorzysta�, je�li nie to stw�rzmy pust� baz� nazywaj�c j� np "tSQLtFramework" i zainstalujmy w niej tSQLt tak jak by�o to opisane w module pierwszym. 
5. Teraz do projektu bazy z testami zaimportujemy baz� zawieraj�c� zainstalowany czysty framework tSQLt. W tym celu korzystamy z opcji "Import -> Database..." tak jak robili�my to dla projektu z baz� produkcyjn�. Je�li wszystko przebieg�o pomy�lnie to do projektu powinna doda� si� referencja do tSQLtCLR oraz powinna stworzy� si� odpowiednia struktura folder�w zawieraj�ca schem� tSQLt i niezb�dne obiekty.
6. Po zaimportowaniu tSQLt do projektu Visual Studio podkre�li nam, �e w projekcie wyst�puj� b��dy. Wynika to z faktu, �e tSQLt wykorzystuje obiekty bazy master zatem dodajmy jeszcze referencj� do bazy master. Wybieramy "Add Database Reference..." w znanym ju� nam oknie w sekcji "Database Reference" wybieramy "System database" a nast�pnie wskazujemy baz� danych "master". W sekcji "Database Location" wybieramy "different database, same server" i klikamy "OK". Po dodaniu referencji po kr�tkiej chwili Visual Studio powinien ukry� podkre�lenie wskazuj�ce na b��dy w projekcie.
7. Ostatnim krokiem przygotowania naszej bazy danych b�dzie zapewnienie �eby projekt uda�o si� opublikowa� na serwerze SQL. W tym celu musimy doda� skrypt kt�ry b�dzie wywo�any przed deployem bazy danych a jego zadaniem b�dzie w��czenie CLR'a dla bazy danych. 
8. Klikamy na projekcie bazy testowej prawym przyciskiem i wybieramy "Add -> Script..." w wy�wietlonym oknie odnajdujemy template "Pre-Deployment Script" a do dodanego do projektu skryptu wklejamy poni�szy kod: 

sp_configure 'clr enabled', 1;
GO
RECONFIGURE;
GO

9. Teraz mo�emy spokojnie klikn�� na naszym projekcie prawym przyciskiem myszy wybra� opcj� "Publish" i zainstalowa� baz� z zainstalowanym w niej frameworkiem tSQLt.

**Tworzenie i aktualizacja zestawu test�w**

Testy mo�emy dodawa� bezpo�rednio w projekcie bazodanowym w Visual Studio lub dodawa� wprost na bazie danych i po ich sko�czeniu aktualizowa� projekt bazodanowy w Visual Studio korzystaj�c z opcji "Schema Compare"

1. Dodajmy w SSMS now� klas� testow� i prosty test a nast�pnie sprawd�my czy testy w naszej nowej bazie uruchamiaj� si� poprawnie - mo�esz wykorzysta� poni�szy skrypt: 

EXEC tSQLt.NewTestClass 'StorageTests'
GO

CREATE PROCEDURE StorageTests.[test if Order table is empty after deploy]
AS
BEGIN
	--Given
 
	--When
 
	--Then
	EXEC tSQLt.AssertEmptyTable 'Storage.Order'
END;
GO
 
EXEC tSQLt.RunAll

2. Zaaplikujmy zmiany wprowadzone bezpo�rednio w bazie do projektu. Wybieramy na  projekcie opcj� "Schema Compare..." w wy�wietlonym oknie jako �r�d�o wskazujemy nasz� baz� danych a jako cel wskazujemy nasz projekt bazodanowy z testami i klikamy "Compare". W oknie wy�wietlona zostanie lista zmian czyli dodana przez nas klasa testowa i test, wybieramy "Update" i po chwili mamy wszystkie zmiany naniesione na projekt.

3. W przypadku tworzenia test�w bezpo�rednio z poziomu Visual Studio trzeba tylko pami�ta�, �e klasa testowa to schema z dodatkow� w�a��iwo�ci�. Trzeba wi�c zadba� o to w skryptach tworz�cych scheme'y, kt�re maj� by� rozpoznane przez tSQLt jako klasy testowe. Poni�ej przyk�ad:

CREATE SCHEMA [StorageTests] AUTHORIZATION [dbo];
GO

EXECUTE sp_addextendedproperty @name = N'tSQLt.TestClass', @value = 1, @level0type = N'SCHEMA', @level0name = N'StorageTests';

