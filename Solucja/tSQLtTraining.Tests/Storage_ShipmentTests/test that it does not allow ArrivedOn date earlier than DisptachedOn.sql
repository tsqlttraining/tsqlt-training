﻿CREATE PROCEDURE Storage_ShipmentTests.[test that it does not allow ArrivedOn date earlier than DisptachedOn]
AS
BEGIN
	-- Given
	DECLARE @constraintName NVARCHAR(50) = 'CK_Shipment_ArrivedOn_LaterOrEqualTo_DispatchedOn'
	DECLARE @expectedErrorMessagePattern NVARCHAR(MAX) = N'The INSERT statement conflicted with the CHECK constraint "' + @constraintName + '"%'

	EXEC tSQLt.FakeTable 'Storage.Shipment'
	EXEC tSQLt.ApplyConstraint @TableName = 'Storage.Shipment', @ConstraintName = @constraintName

	DECLARE @dispatchedOn DATETIME2 = '2017-11-01'
	DECLARE @arrivedOn DATETIME2 = '2017-10-01'
	
	-- When
	-- Then
	EXEC tSQLt.ExpectException @ExpectedMessagePattern = @expectedErrorMessagePattern

	INSERT INTO Storage.Shipment (DispatchedOn, ArrivedOn)
	VALUES (@dispatchedOn, @arrivedOn)
END
