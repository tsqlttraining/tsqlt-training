﻿CREATE PROCEDURE Storage_WarehouseInventoryTests.[test that it logs with current date]
AS
BEGIN
	-- Given
	EXEC tSQLt.FakeTable 'Storage.WarehouseInventory'
	EXEC tSQLt.ApplyTrigger @TableName = 'Storage.WarehouseInventory',
		@TriggerName = 'Storage.Trigger_WarehouseInventory_ProductQuantityHistory'
	EXEC tSQLt.FakeFunction 'utils.[GetSysDateTime]', 'TestUtils.Fake_GetSysDateTime'

	DECLARE @currentDate DATETIME2(7) = '2017-10-31 23:59:59.111'

	INSERT INTO TestUtils.[Fake_GetSysDateTimeDataSource] (FakeDate)
	VALUES (@currentDate)

	-- When
	INSERT INTO Storage.WarehouseInventory (WarehouseId, ProductId, Quantity)
	VALUES (1, 1, 10),
		(2, 2, 20)

	-- Then

	SELECT WarehouseId, ProductId, NewQuantity, PreviousQuantity, ChangedOn
	INTO actual
	FROM Storage.WarehouseInventoryHistory

	SELECT 1 AS WarehouseId, 1 AS ProductId, 10 AS NewQuantity, NULL AS PreviousQuantity, @currentDate AS ChangedOn
	INTO expected
	UNION
	SELECT 2, 2, 20, NULL, @currentDate

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END

