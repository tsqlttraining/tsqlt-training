﻿CREATE PROCEDURE Storage_WarehouseInventoryTests.[test that it logs when product is deleted from inventory]
AS
BEGIN
	-- Given
	EXEC tSQLt.FakeTable 'Storage.WarehouseInventory'

	INSERT INTO Storage.WarehouseInventory (WarehouseId, ProductId, Quantity)
	VALUES (1, 1, 10),
		(2, 2, 20)

	EXEC tSQLt.ApplyTrigger @TableName = 'Storage.WarehouseInventory', @TriggerName = 'Storage.Trigger_WarehouseInventory_ProductQuantityHistory'

	-- When
	DELETE FROM Storage.WarehouseInventory

	-- Then
	SELECT WarehouseId, ProductId, NewQuantity, PreviousQuantity
	INTO actual
	FROM Storage.WarehouseInventoryHistory

	SELECT 1 AS WarehouseId, 1 AS ProductId, NULL AS NewQuantity, 10 AS PreviousQuantity
	INTO expected
	UNION
	SELECT 2, 2, NULL, 20

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END
