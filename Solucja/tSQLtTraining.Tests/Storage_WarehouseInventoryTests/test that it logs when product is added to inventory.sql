﻿CREATE PROCEDURE Storage_WarehouseInventoryTests.[test that it logs when product is added to inventory]
AS
BEGIN
	-- Given
	EXEC tSQLt.FakeTable 'Storage.WarehouseInventory'
	EXEC tSQLt.ApplyTrigger @TableName = 'Storage.WarehouseInventory', @TriggerName = 'Storage.Trigger_WarehouseInventory_ProductQuantityHistory'

	-- When
	INSERT INTO Storage.WarehouseInventory (WarehouseId, ProductId, Quantity)
	VALUES (1, 1, 10),
		(2, 2, 20)

	-- Then
	SELECT WarehouseId, ProductId, NewQuantity, PreviousQuantity
	INTO actual
	FROM Storage.WarehouseInventoryHistory

	SELECT 1 AS WarehouseId, 1 AS ProductId, 10 AS NewQuantity, NULL AS PreviousQuantity
	INTO expected
	UNION
	SELECT 2, 2, 20, NULL

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END

