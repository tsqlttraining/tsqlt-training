﻿CREATE PROCEDURE Storage_ShipOrderProductsTests.[test that it throws error when provided order does not exist]
AS
BEGIN
	EXEC tSQLt.FakeTable 'Storage.Order'

	EXEC tSQLt.ExpectException @ExpectedMessage = 'Cannot ship not existing product!', @ExpectedSeverity = 16
	EXEC Storage.ShipOrderProducts @orderId = 0
END
