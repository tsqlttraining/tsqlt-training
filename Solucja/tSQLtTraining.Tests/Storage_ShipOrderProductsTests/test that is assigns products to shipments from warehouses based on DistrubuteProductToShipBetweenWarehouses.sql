﻿CREATE PROCEDURE Storage_ShipOrderProductsTests.[test that is assigns products to shipments from warehouses based on DistrubuteProductToShipBetweenWarehouses]
AS
BEGIN
	EXEC tSQLt.FakeTable 'Storage.Order'
	EXEC tSQLt.FakeTable 'Storage.OrderShipment'
	EXEC tSQLt.FakeTable 'Storage.Shipment', @Identity = 1
	EXEC tSQLt.FakeTable 'Storage.ShipmentProduct'
	EXEC tSQLt.FakeFunction 'Storage.DistributeProductToShipBetweenWarehouses', 'Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses'

	DECLARE @orderId INT = 1
	DECLARE @productId INT = 1
	DECLARE @orderWarehouseId INT = 1

	INSERT INTO Storage.[Order] (OrderId, ProductId, DestinationWarehouseId, Quantity)
	VALUES (@orderId, @productId, @orderWarehouseId, 10)

	INSERT INTO Storage_ShipOrderProductsTests.Fake_ProductDistribution (WarehouseId, Quantity)
	VALUES (2, 5), (3, 3), (4, 2)

	EXEC Storage.ShipOrderProducts @orderId = @orderId

	SELECT s.SourceWarehouseId, s.DestinationWarehouseId, sp.ProductId, sp.Quantity
	INTO actual
	FROM Storage.ShipmentProduct AS sp
	JOIN Storage.Shipment AS s ON s.ShipmentId = sp.ShipmentId

	CREATE TABLE expected (
		SourceWarehouseId INT,
		DestinationWarehouseId INT,
		ProductId INT,
		Quantity INT
	)

	INSERT INTO expected
	VALUES (2, @orderWarehouseId, @productId, 5),
		(3, @orderWarehouseId, @productId, 3),
		(4, @orderWarehouseId, @productId, 2)

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END