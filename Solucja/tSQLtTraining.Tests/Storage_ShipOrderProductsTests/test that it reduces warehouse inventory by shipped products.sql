﻿CREATE PROCEDURE Storage_ShipOrderProductsTests.[test that it reduces warehouse inventory by shipped products]
AS
BEGIN
	EXEC tSQLt.FakeTable 'Storage.Order'
	EXEC tSQLt.FakeTable 'Storage.OrderShipment'
	EXEC tSQLt.FakeTable 'Storage.Shipment', @Identity = 1
	EXEC tSQLt.FakeTable 'Storage.ShipmentProduct'
	EXEC tSQLt.FakeTable 'Storage.WarehouseInventory'
	EXEC tSQLt.FakeFunction 'Storage.DistributeProductToShipBetweenWarehouses', 'Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses'

	DECLARE @orderId INT = 1
	DECLARE @productId INT = 1
	DECLARE @orderWarehouseId INT = 1

	INSERT INTO Storage.[Order] (OrderId, ProductId, DestinationWarehouseId, Quantity)
	VALUES (@orderId, @productId, @orderWarehouseId, 10)

	INSERT INTO Storage_ShipOrderProductsTests.Fake_ProductDistribution (WarehouseId, Quantity)
	VALUES (2, 9), (3, 7), (4, 6)

	INSERT INTO Storage.WarehouseInventory (WarehouseId, ProductId, Quantity)
	VALUES (2, @productId, 19), (3, @productId,  17), (4, @productId, 16)

	EXEC Storage.ShipOrderProducts @orderId = @orderId

	SELECT wi.WarehouseId, wi.ProductId, wi.Quantity
	INTO actual
	FROM Storage.WarehouseInventory AS wi

	CREATE TABLE expected (
		WarehouseId INT,
		ProductId INT,
		Quantity INT
	)

	INSERT INTO expected
	VALUES (2, @productId, 10),
		(3, @productId, 10),
		(4, @productId, 10)

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END