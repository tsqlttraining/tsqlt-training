﻿CREATE FUNCTION Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses
(
	@productId INT,
	@desiredQuantity INT,
	@excludedWarehouseId INT
)
RETURNS TABLE AS RETURN
(
	SELECT WarehouseId, Quantity AS QuantityToShip
	FROM Storage_ShipOrderProductsTests.Fake_ProductDistribution
)