﻿CREATE PROCEDURE Storage_ShipOrderProductsTests.[test that it adds new shippments when no pending shipments match order]
AS
BEGIN
	EXEC tSQLt.FakeTable 'Storage.Order'
	EXEC tSQLt.FakeTable 'Storage.Shipment', @Identity = 1
	EXEC tSQLt.FakeTable 'Storage.ShipmentProduct'
	EXEC tSQLt.FakeTable 'Storage.OrderShipment'
	EXEC tSQLt.FakeTable 'Storage.WarehouseInventory'
	EXEC tSQLt.FakeFunction 'Storage.DistributeProductToShipBetweenWarehouses', 'Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses'
	
	DECLARE @orderId INT = 1

	INSERT INTO Storage.[Order] (OrderId, ProductId, Quantity, DestinationWarehouseId)
	VALUES (@orderId, 1, 10, 1)

	INSERT INTO Storage_ShipOrderProductsTests.Fake_ProductDistribution (WarehouseId, Quantity)
	VALUES (2, 5), (3, 3), (4, 2)

	EXEC Storage.ShipOrderProducts @orderId = @orderId
	
	SELECT SourceWarehouseId, DestinationWarehouseId
	INTO actual
	FROM Storage.Shipment AS s
	WHERE s.ArrivedOn IS NULL AND s.DispatchedOn IS NULL

	CREATE TABLE expected (
		SourceWarehouseId INT,
		DestinationWarehouseId INT
	)

	INSERT INTO expected (SourceWarehouseId, DestinationWarehouseId)
	VALUES (2, 1), (3, 1), (4, 1)

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END
