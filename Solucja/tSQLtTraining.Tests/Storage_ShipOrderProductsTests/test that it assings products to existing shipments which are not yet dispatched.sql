﻿CREATE PROCEDURE Storage_ShipOrderProductsTests.[test that it assings products to existing shipments which are not yet dispatched]
AS
BEGIN
	EXEC tSQLt.FakeTable 'Storage.Order'
	EXEC tSQLt.FakeTable 'Storage.Shipment'
	EXEC tSQLt.FakeTable 'Storage.ShipmentProduct'
	EXEC tSQLt.FakeTable 'Storage.OrderShipment'
	EXEC tSQLt.FakeTable 'Storage.WarehouseInventory'
	EXEC tSQLt.FakeFunction 'Storage.DistributeProductToShipBetweenWarehouses', 'Storage_ShipOrderProductsTests.Fake_ProductDistributionBetweenWarehouses'
	
	DECLARE @orderId INT = 1
	DECLARE @productId INT = 1
	DECLARE @warehouseId INT = 1

	INSERT INTO Storage.[Order] (OrderId, ProductId, Quantity, DestinationWarehouseId)
	VALUES (@orderId, @productId, 10, 1)

	INSERT INTO Storage_ShipOrderProductsTests.Fake_ProductDistribution (WarehouseId, Quantity)
	VALUES (2, 5), (3, 3), (4, 2)

	INSERT INTO Storage.Shipment (ShipmentId, SourceWarehouseId, DestinationWarehouseId)
	VALUES (1, 2, @warehouseId),
		(2, 3, @warehouseId),
		(3, 4, @warehouseId)

	INSERT INTO Storage.ShipmentProduct (ShipmentId, ProductId, Quantity)
	VALUES (1, @productId, 10),
		(2, @productId, 20),
		(3, @productId, 30)

	EXEC Storage.ShipOrderProducts @orderId = @orderId
	
	SELECT s.SourceWarehouseId, s.DestinationWarehouseId, sp.ProductId, sp.Quantity
	INTO actual
	FROM Storage.ShipmentProduct AS sp
	JOIN Storage.Shipment AS s ON s.ShipmentId = sp.ShipmentId

	CREATE TABLE expected (
		SourceWarehouseId INT,
		DestinationWarehouseId INT,
		ProductId INT,
		Quantity INT
	)

	INSERT INTO expected (SourceWarehouseId, DestinationWarehouseId, ProductId, Quantity)
	VALUES (2, @warehouseId, @productId, 15),
		(3, @warehouseId, @productId, 23),
		(4, @warehouseId, @productId, 32)

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END
