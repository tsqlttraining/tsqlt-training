﻿CREATE PROCEDURE Storage_OutgoingWarehouseShipmentsTests.[test that it returns number of outgoing shipments per warehouse]
AS
BEGIN
	-- Given
	EXEC tSQLt.FakeTable 'Storage.Warehouse'
	EXEC tSQLt.FakeTable 'Storage.Shipment', @Identity = 1

	INSERT INTO Storage.Warehouse(WarehouseId) VALUES (1), (2), (3)

	INSERT INTO Storage.Shipment (SourceWarehouseId, ArrivedOn)
	VALUES (1, NULL),
		(1, NULL),
		(1, GETDATE()),
		(2, NULL),
		(2, NULL),
		(2, NULL)
	
	-- When
	SELECT *
	INTO actual
	FROM Storage.OutgoingWarehouseShipments

	-- Then
	CREATE TABLE expected (
		WarehouseId INT,
		NumberOfShipments INT,
	)

	INSERT INTO expected
	VALUES (1, 2),
		(2, 3),
		(3, 0)

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END
