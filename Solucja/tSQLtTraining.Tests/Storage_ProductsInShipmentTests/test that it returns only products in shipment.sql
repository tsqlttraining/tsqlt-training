﻿CREATE PROCEDURE Storage_ProductsInShipmentTests.[test that it returns only products in shipment]
AS
BEGIN
	-- Given
	EXEC tSQLt.FakeTable 'Production.Product'
	EXEC tSQLt.FakeTable 'Storage.Shipment'
	EXEC tSQLt.FakeTable 'Storage.ShipmentProduct'

	INSERT INTO Production.Product (ProductId, [Name])
	VALUES (1, 'Paper towel'),
		(2, 'Rubber duck'),
		(3, 'Scissors'),
		(4, 'Not Shipped Product')

	INSERT INTO Storage.Shipment (ShipmentId, DispatchedOn, ArrivedOn)
	VALUES (1,'2017-11-01 14:10',null),
		(2,'2016-10-30 09:35',null),
		(3,'2016-11-02 11:32',null),
		(4,'2016-11-03 12:45',null),
		(5,'2016-11-03 12:45','2017-11-03 12:45')

	INSERT INTO Storage.ShipmentProduct (ShipmentId, ProductId, Quantity)
	VALUES (1,1, 160),
		(2,1,50),
		(3,2,42),
		(4,3,30),
		(5,1,160)
	
	-- When
	SELECT *
	INTO actual
	FROM Storage.ProductsInShipment

	-- Then
	CREATE TABLE expected (
		ProductName NVARCHAR(60),
		Quantity INT,
		DispatchedOn DATETIME2,
	)

	INSERT INTO expected
	VALUES ('Paper towel', 160, '2017-11-01 14:10'),
		('Paper Towel', 50, '2016-10-30 09:35'),
		('Rubber duck', 42, '2016-11-02 11:32'),
		('Scissors', 30, '2016-11-03 12:45')

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END
