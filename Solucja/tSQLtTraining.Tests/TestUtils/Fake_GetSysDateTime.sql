﻿CREATE FUNCTION TestUtils.[Fake_GetSysDateTime]()
RETURNS DATETIME2(7)
AS
BEGIN
	DECLARE @fakeDate DATETIME2 (7);
	SELECT TOP 1 @fakeDate = FakeDate
	FROM TestUtils.Fake_GetSysDateTimeDataSource

	RETURN @fakeDate
END
