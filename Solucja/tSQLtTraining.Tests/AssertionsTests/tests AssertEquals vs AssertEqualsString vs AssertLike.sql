﻿
CREATE PROCEDURE AssertionsTests.[tests AssertEquals vs AssertEqualsString vs AssertLike]
AS
BEGIN
	--Assemble
	DECLARE @a VARCHAR(30) = 'test     '
	DECLARE @b NVARCHAR(40) = N'test '

	DECLARE @bigA VARCHAR(MAX) = 'test    '
	DECLARE @bigB NVARCHAR(MAX) = N'test '

	--Act

	--Assert

	EXEC tSQLt.AssertEquals @a, @b
	EXEC tSQLt.AssertEqualsString @bigA, @bigB
END