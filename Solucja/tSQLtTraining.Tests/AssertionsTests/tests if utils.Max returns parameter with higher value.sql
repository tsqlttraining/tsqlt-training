﻿
CREATE PROCEDURE AssertionsTests.[tests if utils.Max returns parameter with higher value]
AS
BEGIN
	--Assemble
	DECLARE @smaller INT = 1
	DECLARE @larger INT = 5

	--Act
	DECLARE @result INT

	SELECT @result = utils.[Max](@smaller, @larger)

	--Assert
	EXEC tSQLt.AssertEquals @larger, @result
END