﻿
CREATE PROCEDURE AssertionsTests.[test exception is raised when adding non-unique Product]
AS
BEGIN
	--Assemble
	EXEC tSQLt.ExpectException @ExpectedMessage = 'Cannot create same product (Product Name) twice!',
	 @ExpectedSeverity = 16,
	 @ExpectedState = 1

	--Act
	EXEC Production.AddNewProduct @ProductName = 'Product Name'
	EXEC Production.AddNewProduct @ProductName = 'Product Name'

	--Assert

END