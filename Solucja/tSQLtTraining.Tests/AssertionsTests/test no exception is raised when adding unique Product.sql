﻿
CREATE PROCEDURE AssertionsTests.[test no exception is raised when adding unique Product]
AS
BEGIN
	--Assemble
	EXEC tSQLt.ExpectNoException
	
	--Act
	EXEC Production.AddNewProduct @ProductName = 'Product Name'

	--Assert

END