﻿CREATE PROCEDURE Storage_ShipAllOrdersTests.[test that it executes ShipOrderProducts for every order to ship]
AS
BEGIN
	EXEC tSQLt.FakeTable 'Storage.ProductsToShip'

	EXEC tSQLt.SpyProcedure 'Storage.ShipOrderProducts'

	INSERT INTO Storage.ProductsToShip (OrderId)
	VALUES (1), (2), (3)

	EXEC Storage.ShipAllOrders

	SELECT orderId
    INTO actual
    FROM Storage.ShipOrderProducts_SpyProcedureLog;

	SELECT 1 AS orderId
	INTO expected
	UNION
	SELECT 2
	UNION
	SELECT 3

	EXEC tSQLt.AssertEqualsTable 'expected', 'actual'
END
