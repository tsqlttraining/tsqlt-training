﻿CREATE PROCEDURE [Storage].[MigrateByRunningSSISPackage]
AS
BEGIN
	DECLARE @execution_id BIGINT

	EXEC [SSISDB].[catalog].[create_execution] @package_name = N'MigrateWarehouseData.dtsx'
		,@execution_id = @execution_id OUTPUT
		,@folder_name = N'tSQLtTraining'
		,@project_name = N'tSQLtTraining_MigrationSSIS_NT'
		,@use32bitruntime = False
		,@reference_id = NULL

	DECLARE @var0 SMALLINT = 1

	EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id
		,@object_type = 50
		,@parameter_name = N'SYNCHRONIZED'
		,@parameter_value = 1

	EXEC [SSISDB].[catalog].[set_execution_parameter_value] @execution_id
		,@object_type = 50
		,@parameter_name = N'LOGGING_LEVEL'
		,@parameter_value = @var0

	EXEC [SSISDB].[catalog].[start_execution] @execution_id
END