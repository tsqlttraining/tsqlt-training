﻿CREATE PROCEDURE [Storage].[MigrateByCopyingDataManually]
AS
BEGIN
	SET IDENTITY_INSERT Storage.Warehouse ON

	MERGE Storage.Warehouse AS [target]
	USING tSQLTTraining_SourceDB.Storage.WarehousesToMigrate AS [source]
	ON ([target].WarehouseId = [source].WarehouseId)
	WHEN NOT MATCHED
		THEN
			INSERT (
				 WarehouseId
				,[Name]
				,Country
				,FullAddress
				)
			VALUES (
				[source].WarehouseId
				,[source].[Name]
				,[source].Country
				,[source].FullAddress
				);
	
	SET IDENTITY_INSERT Storage.Warehouse OFF

	RETURN 0
END