﻿CREATE PROCEDURE SSISTests.[test if Warehouse data is empty when there was no source data] 
AS
BEGIN
    -- Given

    -- When
    EXEC Storage.MigrateByRunningSSISPackage;

    -- Then
    EXEC tSQLt.AssertEmptyTable 'Storage.Warehouse'

END
