﻿CREATE PROCEDURE SSISTests.SetUp
AS
BEGIN
	DELETE
	FROM tSQLtTraining_SourceDB_NT.Storage.WarehousesToMigrate;

	DELETE
	FROM tSQLtTraining_DestinationDB_NT.Storage.Warehouse;
END
